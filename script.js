(() => {
	const host = 'https://denisgidin.ru/freelance/video-recorder';
	const scripts = [
		'/js/videojs/video.min.js',
		'/js/videojs/RecordRTC.js',
		'/js/videojs/adapter.js',
		'/js/videojs/videojs.record.js',
		'/js/myAppName.js'];

	const styles = [
		'/css/videojs/video-js.min.css',
		'/css/videojs/videojs.record.css',
		'/css/myAppName.css',
		'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css']

	styles.forEach(url => {
		const linkElement = this.document.createElement('link');
		linkElement.setAttribute('rel', 'stylesheet');
		linkElement.setAttribute('type', 'text/css');
		linkElement.setAttribute('href', (url.indexOf('://') == -1 ? host + url : url) + '?t=' + new Date().getTime());
		document.getElementsByTagName('head')[0].appendChild(linkElement);
	});

	for (let i = 0, p = Promise.resolve(); i < scripts.length; i++) {
		p = p.then(_ => new Promise(resolve => {
			const script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = host + scripts[i] + '?t=' + new Date().getTime();
			script.onload = () => {
				console.log(scripts[i]);
				resolve();
			}
			document.getElementsByTagName('head')[0].appendChild(script);
		}
		));
	}

})();
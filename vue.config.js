module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? process.env.VUE_APP_SERVER
    : '/',
  configureWebpack: {
    optimization: {
      splitChunks: false
    }
  },
  filenameHashing: false,
  pages: {
    myAppName: {
      entry: 'src/main.js',
      filename: 'index.html'
    }
  }
}